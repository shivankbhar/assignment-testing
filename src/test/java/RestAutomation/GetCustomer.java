package RestAutomation;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import io.restassured.RestAssured;
import io.restassured.response.Response;

public class GetCustomer {
	
	private String url;
	
	@Parameters({"baseUrl"})
	@BeforeTest
	public void init(String baseUrl) {
		
		this.url = baseUrl + "/customers";
		
	}
	
	@Test 
	public void testRequestStatus() {
		
		Response request = RestAssured.get(url + "/1");
		int code = request.statusCode();
		Assert.assertEquals(code, 200);

	}

	@Test 
	public void testRequestJSON() {
		
		Response request = RestAssured.get(url+ "/1");
		String type = request.contentType();
		Assert.assertTrue(type.contains("json"));

	}

	@Test 
	public void testRequestNotFound() {
		
		Response request = RestAssured.get(this.url+ "/InvalidId");
		int code = request.statusCode();
		Assert.assertEquals(code, 404);

	}
	
	@Parameters({"attribute","attributeValue"})
	@Test 
	public void testRequestValidData(String attribute, String attributeValue) {
		
		Response request = RestAssured.get(this.url+ "/1");
		String customerAttribute =   request.body().path("data."+attribute);
		Assert.assertEquals(customerAttribute , attributeValue);

	}

}