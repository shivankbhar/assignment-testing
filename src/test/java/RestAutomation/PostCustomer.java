package RestAutomation;

import org.json.simple.JSONObject;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class PostCustomer {
	
	private String url;
	private String authCookie;
	
	@Parameters({"baseUrl","authCookie"})
	@BeforeTest
	public void init(String baseUrl, String authCookie) {
		
		this.url = baseUrl + "/customers";
		this.authCookie = authCookie;
		
	}

	@Test 
	public void testRequestStatus() {

		RequestSpecification request = RestAssured.given();
		request.header("content-type","application/json");
		request.header("Authorization",authCookie);
		
		JSONObject requestParams = new JSONObject();
			
		requestParams.put("name", "John Doe");
		requestParams.put("dob", "15-04-1994");
		
		request.body(requestParams.toJSONString());
		Response response = request.post(url);
		int code = response.statusCode();

		Assert.assertEquals(code, 201);
		
	}


	@Test 
	public void testRequestBadRequest() {

		RequestSpecification request = RestAssured.given();
		request.header("content-type","application/json");
		request.header("Authorization",authCookie);
		
		request.body("Invalid data type");

		Response response = request.post(url);
		int code = response.statusCode();

		Assert.assertEquals(code, 400);
		

	}
	
	@Test 
	public void testRequestUnauthorized() {

		RequestSpecification request = RestAssured.given();
		request.header("content-type","application/json");
		request.header("Authorization","Invalid Auth Cookie");
		JSONObject requestParams = new JSONObject();

		requestParams.put("name", "Jane Doe");
		requestParams.put("dob", "15-04-1994");
		
		request.body(requestParams.toJSONString());
		Response response = request.post(url);
		int code = response.statusCode();

		Assert.assertEquals(code, 401);
		
	}
	
}